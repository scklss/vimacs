" General -------- {{{
set clipboard=unnamedplus
syntax on
filetype plugin on
set nocompatible
set encoding=utf-8
set nu
set rnu
set noerrorbells
set noshowmode
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set ignorecase
set smartcase
set noswapfile
set nobackup
set incsearch
set hlsearch
set hidden
set scrolloff=8
set wildmenu
set foldmethod=marker
set autowrite
set nrformats+=alpha
set mouse=a
set virtualedit=block
let mapleader = ' '
" }}}

" Smooth wrapping with indent -------- {{{
set wrap nolist linebreak
set breakindent 
set breakindentopt=shift:2
" }}}

" Basic mappings -------- {{{ 
nnoremap <Leader>v :tabedit $MYVIMRC<CR>
nnoremap <Leader>so :so $MYVIMRC<CR>
nnoremap <Leader>e :Lex!<CR>
" }}}

" Autocommands -------- {{{
augroup VIMRC
    autocmd!
    autocmd FileType help nnoremap <buffer> q :helpclose<cr>
    autocmd FileType netrw setlocal statusline=NETRW
augroup END
" }}}

" Default fuzzy find {{{
set path+=**
set fileignorecase
set wildignore+=**/_resources/**
" }}}

" Path completion {{{
set completeopt=menu,noselect,noinsert
inoremap <C-f> <C-x><C-f>
inoremap <expr> <CR> pumvisible() ? "\<C-x><C-f>" : "\<CR>"
" }}}

" Window movement {{{
nnoremap <Leader>h :wincmd h<CR>
nnoremap <Leader>j :wincmd j<CR>
nnoremap <Leader>k :wincmd k<CR>
nnoremap <Leader>l :wincmd l<CR>
" }}}

" Move lines in Visual mode {{{
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=g
" }}}

" Abbreviations {{{
iabbrev bbash #!/bin/bash
iabbrev bsh #!/bin/sh
" }}}

" Status Line {{{
function! GetCurrentMode() abort
    let l:ModeDict = {'n': 'Normal', 'i': 'Insert', 'v': 'Visual', 'V': 'V-Line', '': 'V-Block', 'c': 'Command'}
    let l:CurrentModeSymbol = mode()
    let l:CurrentMode = toupper(get(l:ModeDict, l:CurrentModeSymbol, l:CurrentModeSymbol))
    return l:CurrentMode
endfunction

hi User1 ctermbg=Black ctermfg=White cterm=BOLD
hi User2 ctermbg=DarkGray ctermfg=White cterm=BOLD

set statusline=%1*                          "User1 Color
set statusline+=\                        	"Initial Space 
set statusline+=%{GetCurrentMode()}
set statusline+=\                    	    "Space
set statusline+=%2*                         "User2 Color
set statusline+=\                    	    "Space
set statusline+=%f                      	"Path of file
set statusline+=%m                      	"Modified[+]
set statusline+=\ -\                    	"Separator
set statusline+=%{FugitiveStatusline()}
set statusline+=%y                      	"Filetype of the current file
set statusline+=%=                      	"Switch right side
set statusline+=%1*                         "User1 Color
set statusline+=\                    	    "Space
set statusline+=%P                      	"Percentage
set statusline+=\(                      	"Open parenthesis
set statusline+=%l                      	"Current line number
set statusline+=/                       	"Divider
set statusline+=%L                      	"Total line number
set statusline+=\)                      	"Close parenthesis
set statusline+=\                        	"Ending Space 
" }}}

" Plugins List -------- {{{
call plug#begin('~/.vim/plugged')

" Telescope
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" vimwiki
Plug 'vimwiki/vimwiki'

" tpope
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-surround'

" Emmet
Plug 'mattn/emmet-vim'

"CoC
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

" }}}

" Plugins Config -------- {{{

" Color scheme {{{
" colorscheme gruvbox
set bg=dark
" }}}

" Emmet config {{{
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
" }}}

" Telescope {{{
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr> 
" }}}

" Netrw config {{{
let g:netrw_winsize = 24
let g:netrw_banner = 0
let g:netrw_browsex_viewer= "xdg-open"
let g:netrw_liststyle = 3
" }}}

" }}}

" Source local scripts
exec "source" .. stdpath('config') .. "/scripts/QuickfixMisspelledWords.vim"

" Local scripts mappings
nnoremap <Leader>sp :call QuickfixMisspelledWords()<CR>


