function! QuickfixMisspelledWords() abort
	let l:QFList = []
	let l:Words = GetMisspelledWords()
    if (len(l:Words) == 0)
        echom "No misspelling found."
        return
    else 
        for word in l:Words
            call add(l:QFList, {'bufnr':word['bufnr'], 'lnum':word['lnum'], 'col':word['scol'], 'type':word['type'], 'text':word['word'] .. ' -> ' .. word['sugg']})
        endfor
        call setqflist(l:QFList)
        copen
    endif
endfunction

function! GetMisspelledWords() abort
	let l:LineCnt  = 0
	let l:BufLines = getbufline('%', 1, '$')
	let l:BadWords = []
	for line in l:BufLines
		let l:Start=0
		let l:LineCnt = l:LineCnt + 1
		while v:true
			call cursor(l:LineCnt, l:Start+1)
			let l:CursorPos = getpos('.')
			if l:CursorPos[2] != l:Start+1
				break
			end
			let l:Check = spellbadword()
			if l:Check[0] != ''
				let l:Result = {}
				let l:Result['word']  = l:Check[0]
				let l:Result['type']  = toupper(l:Check[1][0])
				let l:Result['bufnr'] = bufnr('%')
				let l:CursorPos       = getpos('.')
				let l:Result['lnum']  = l:LineCnt
				let l:Start           = l:CursorPos[2]
				let l:Result['scol']  = l:Start
				let l:End             = l:Start + strdisplaywidth(l:Check[0]) - 1
				let l:Result['ecol']  = l:End
				let l:Result['sugg']  = spellsuggest(l:Check[0], 1)[0]
				call add(l:BadWords, l:Result)
				let l:Start = l:End
			else
				break
			end
		endwhile
	endfor
	return l:BadWords
endfunction
